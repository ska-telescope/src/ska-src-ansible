.PHONY: all 

USER_ACCOUNT			?=#									user account name (if applicable)

COMMON_CFG_DIR_ABSPATH          ?= /opt/ska-src-skaosrc-nodes-cfg/common/etc#                           path to common configuration files
WORKING_DIR_ABSPATH             ?= /opt/ska-src-skaosrc-nodes-cfg#                                      where inventories etc. will be kept

# --- Do not edit below here.

ANSIBLE_EXTRA_VARS = --extra-vars working_dir_abspath=$(WORKING_DIR_ABSPATH) 
ifneq ($(USER_ACCOUNT),)
        ANSIBLE_EXTRA_VARS += --extra-vars account=$(USER_ACCOUNT)
endif
ifneq ($(COMMON_CFG_DIR_ABSPATH),)
        ANSIBLE_EXTRA_VARS += --extra-vars common_cfg_dir_abspath=$(COMMON_CFG_DIR_ABSPATH)
endif

pre:

delete-user-all: pre
ifdef USER_ACCOUNT
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./user-management/playbooks/delete_user_all.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv
else
	@echo "Account not defined!"
endif
update-users-all: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./user-management/playbooks/update_users_all.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

