.PHONY: all 

BASTION_TARGET_HOSTS_GROUP	?= bastion_machines#          						the target group name for the bastion in the inventories file
COMMON_CFG_DIR_ABSPATH          ?= /opt/ska-src-skaosrc-nodes-cfg/common/etc#		                path to common configuration files
WORKING_DIR_ABSPATH     	?= /opt/ska-src-skaosrc-nodes-cfg#					where inventories etc. will be kept

# --- Do not edit below here.

ANSIBLE_EXTRA_VARS = --extra-vars working_dir_abspath=$(WORKING_DIR_ABSPATH) \
		     --extra-vars bastion_target_hosts_group=$(BASTION_TARGET_HOSTS_GROUP)
ifneq ($(COMMON_CFG_DIR_ABSPATH),)
        ANSIBLE_EXTRA_VARS += --extra-vars common_cfg_dir_abspath=$(COMMON_CFG_DIR_ABSPATH)
endif

all: create-bastion-security-group-openstack create-bastion-machines-openstack configure-bastion-machines

pre:

create-bastion-security-group-openstack: pre	
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./bastion/playbooks/create_bastion_security_group_openstack.yml -vv

create-bastion-machines-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./bastion/playbooks/create_bastion_machines_openstack.yml -vv

configure-bastion-machines: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./bastion/playbooks/configure_bastion_machines.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv
