#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <filename>"
    exit 1
fi

filename="$1"
current_section=""
declare -A sections

while IFS= read -r line; do
    line="${line%"${line##*[![:space:]]}"}"  # Trim trailing whitespace

    if [[ -z "$line" || "$line" == \#* ]]; then
        continue  # Skip empty lines and comment lines
    fi

    if [[ "$line" =~ ^\[.*\]$ ]]; then
        current_section="${line#[}"
        current_section="${current_section%]}"
        sections["$current_section"]=""
    elif [[ -n "$current_section" ]]; then
        # Extract IP and hostname from the line
        ip=$(echo "$line" | grep -oP 'ansible_host=\K[^ ]+')
        hostname=$(echo "$line" | awk '{print $1}')

        # Generate the reformatted line
        reformatted_line="$ip $hostname.{{ domain }} $hostname"

        # Append reformatted line to current section
        sections["$current_section"]+="$(echo "$reformatted_line" | sed 's/^[[:space:]]*//')"  # Append line to current section
        sections["$current_section"]+="\n"  # Add a newline for readability
    fi
done < "$filename"

# Print out the reformatted sections
for section_name in "${!sections[@]}"; do
    echo -e "${sections[$section_name]}"
done
