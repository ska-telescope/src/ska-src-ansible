.PHONY: all 

DNS_TARGET_HOSTS_GROUP	?= dns_machines#          						the target group name for the bastion in the inventories file
DNS_CFG_DIR_ABSPATH	?= /opt/ska-src-skaosrc-nodes-cfg/dnsmasq/etc#	        		path to dns configuration files
COMMON_CFG_DIR_ABSPATH  ?= /opt/ska-src-skaosrc-nodes-cfg/common/etc#                           path to common configuration files
WORKING_DIR_ABSPATH     ?= /opt/ska-src-skaosrc-nodes-cfg#                                      where inventories etc. will be kept

# --- Do not edit below here.

ANSIBLE_EXTRA_VARS = --extra-vars working_dir_abspath=$(WORKING_DIR_ABSPATH) \
		     --extra-vars dns_target_hosts_group=$(DNS_TARGET_HOSTS_GROUP)
ifneq ($(DNS_CFG_DIR_ABSPATH),)
        ANSIBLE_EXTRA_VARS += --extra-vars dns_cfg_dir_abspath=$(DNS_CFG_DIR_ABSPATH)
endif
ifneq ($(COMMON_CFG_DIR_ABSPATH),)
        ANSIBLE_EXTRA_VARS += --extra-vars common_cfg_dir_abspath=$(COMMON_CFG_DIR_ABSPATH)
endif

all: create-dns-security-group-openstack create-dns-machines-openstack configure-dns-machines install-dnsmasq update-hosts

pre:

create-dns-security-group-openstack: pre	
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./dns/playbooks/create_dns_security_group_openstack.yml -vv

create-dns-machines-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./dns/playbooks/create_dns_machines_openstack.yml -vv

configure-dns-machines: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
        ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./dns/playbooks/configure_dns_machines.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

install-dnsmasq: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./dns/playbooks/install_dnsmasq.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

update-hosts: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./dns/playbooks/update_hosts.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

