.PHONY: all 

PROXY_TARGET_HOSTS_GROUP	?= proxy_machines#          						the target group name for the proxy in the inventories file
PROXY_CFG_DIR_ABSPATH		?= /opt/ska-src-skaosrc-nodes-cfg/haproxy/etc#				path to haproxy configuration files
COMMON_CFG_DIR_ABSPATH          ?= /opt/ska-src-skaosrc-nodes-cfg/common/etc#                           path to common configuration files
WORKING_DIR_ABSPATH             ?= /opt/ska-src-skaosrc-nodes-cfg#                                      where inventories etc. will be kept

# --- Do not edit below here.

ANSIBLE_EXTRA_VARS = --extra-vars working_dir_abspath=$(WORKING_DIR_ABSPATH) \
		     --extra-vars proxy_target_hosts_group=$(PROXY_TARGET_HOSTS_GROUP)
ifneq ($(PROXY_CFG_DIR_ABSPATH),)
        ANSIBLE_EXTRA_VARS += --extra-vars proxy_cfg_dir_abspath=$(PROXY_CFG_DIR_ABSPATH)
endif
ifneq ($(COMMON_CFG_DIR_ABSPATH),)
        ANSIBLE_EXTRA_VARS += --extra-vars common_cfg_dir_abspath=$(COMMON_CFG_DIR_ABSPATH)
endif

all-but-certs: create-proxy-security-group-openstack create-proxy-machines-openstack configure-proxy-machines install-or-update-haproxy

all: all-but-certs install-or-renew-certs

pre:

create-proxy-security-group-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./proxy/playbooks/create_proxy_security_group_openstack.yml -vv

create-proxy-machines-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./proxy/playbooks/create_proxy_machines_openstack.yml -vv

configure-proxy-machines: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
        ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./proxy/playbooks/configure_proxy_machines.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

install-or-update-haproxy: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
       	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./proxy/playbooks/install_or_update_haproxy.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

install-or-renew-certs: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./proxy/playbooks/install_or_renew_certs.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

