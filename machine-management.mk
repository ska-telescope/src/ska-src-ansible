.PHONY: all 

REQUEST_TARGET_HOSTS_GROUP	?= test_machines#          						the target group name for the bastion in the inventories file
COMMON_CFG_DIR_ABSPATH          ?= /opt/ska-src-skaosrc-nodes-cfg/common/etc#                           path to common configuration files
WORKING_DIR_ABSPATH             ?= /opt/ska-src-skaosrc-nodes-cfg#                                      where inventories etc. will be kept

# machine configuration

REQUEST_MACHINE_NAME            ?= srcnet-test-machine-1#
REQUEST_MACHINE_FLOATING_IP     ?= #                                                                    must be available in the pool
REQUEST_MACHINE_IMAGE           ?= rocky-9-nogui#
REQUEST_MACHINE_FLAVOR          ?= l3.tiny#
REQUEST_MACHINE_SSH_USER        ?= root#
REQUEST_MACHINE_KEY_NAME        ?= root-srcnet-infrastructure-provisioner#
REQUEST_MACHINE_NETWORK         ?= SRCNet-network#
REQUEST_MACHINE_AUTO_IP         ?= no

# --- Do not edit below here.

ANSIBLE_EXTRA_VARS = --extra-vars working_dir_abspath=$(WORKING_DIR_ABSPATH) \
		     --extra-vars request_target_hosts_group=$(REQUEST_TARGET_HOSTS_GROUP)
ifneq ($(REQUEST_MACHINE_NAME),) 
	ANSIBLE_EXTRA_VARS += --extra-vars request_machine_name=$(REQUEST_MACHINE_NAME) 
endif
ifneq ($(REQUEST_MACHINE_FLOATING_IP),)
        ANSIBLE_EXTRA_VARS += --extra-vars request_machine_floating_ip=$(REQUEST_MACHINE_FLOATING_IP)
endif
ifneq ($(REQUEST_MACHINE_IMAGE),)
        ANSIBLE_EXTRA_VARS += --extra-vars request_machine_image=$(REQUEST_MACHINE_IMAGE)
endif
ifneq ($(REQUEST_MACHINE_FLAVOR),)
        ANSIBLE_EXTRA_VARS += --extra-vars request_machine_flavor=$(REQUEST_MACHINE_FLAVOR)
endif
ifneq ($(REQUEST_MACHINE_SSH_USER),)
        ANSIBLE_EXTRA_VARS += --extra-vars request_machine_ssh_user=$(REQUEST_MACHINE_SSH_USER)
endif
ifneq ($(REQUEST_MACHINE_KEY_NAME),)
        ANSIBLE_EXTRA_VARS += --extra-vars request_machine_key_name=$(REQUEST_MACHINE_KEY_NAME)
endif
ifneq ($(iREQUEST_MACHINE_NETWORK),)
        ANSIBLE_EXTRA_VARS += --extra-vars request_machine_network=$(REQUEST_MACHINE_NETWORK)
endif
ifneq ($(REQUEST_MACHINE_AUTO_IP),)
        ANSIBLE_EXTRA_VARS += --extra-vars request_machine_auto_ip=$(REQUEST_MACHINE_AUTO_IP)
endif
ifneq ($(COMMON_CFG_DIR_ABSPATH),)
        ANSIBLE_EXTRA_VARS += --extra-vars common_cfg_dir_abspath=$(COMMON_CFG_DIR_ABSPATH)
endif

create-and-configure-new-machine: create-general-machine-openstack configure-machine

pre:

create-general-machine-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./machine-management/playbooks/create_general_machine_openstack.yml -vv

delete-general-machine-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./machine-management/playbooks/delete_general_machine_openstack.yml -vv

configure-machine: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./machine-management/playbooks/configure_machine.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

