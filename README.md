# ska-skaosrc-nodes-iac

[[_TOC_]]

This repository contains Ansible playbooks and Makefiles to build the infrastructure for SKAOSRC.

## Prerequisites

It is assumed that the Makefiles discussed herein are being run from a dedicated "infrastructure provisioning" machine. 
Having a machine dedicated solely for this purpose makes it less likely that the resulting machine inventory will become 
inconsistent across deployments, although the inventory itself is (by default) kept in a separate configuration 
repository (`skaosrc/ska-skaosrc-nodes-cfg`), so divergence can be minimised if this is always committed following any changes.

### Creating an infrastructure provisioning machine (manual)

Currently the infrastructure provisioning machine must be created manually. The recipe (for RHEL distros) looks something like:

1. Create a new VM manually and add required user keys to `.ssh/authorized_keys` file (keep as single superuser).

2. SSH on to this machine and install ansible:
   
    ```bash
    $ yum install ansible
    ```

3. Install the Openstack ansible collection (`ansible-galaxy collection install git+https://opendev.org/openstack/ansible-collections-openstack`). 

4. Install the Openstack SDK w/ CLI & bash completion for ease of use:

    ```bash
   $ yum install python3-openstacksdk python3-openstackclient bash-completion
   ```

5. Create a new Openstack application credential for this machine from the Openstack web GUI (`identity > application credentials`) and download the resulting `clouds.yaml` file.

6. Create a copy of this application credential in `/etc/openstack/clouds.yaml` to enable communication with the Openstack API via the CLI.

7. Add a duplicate `clouds.capi.yaml` in `/etc/openstack` containing the `cacert` key required by the clusterapi roles. This is just the `clouds.yaml` from 6. but with the following addition:

    ```yaml
    cacert: |
      -----BEGIN CERTIFICATE-----
      MIIJUzCCBzugAwIBAgIQRwOt02OLz0HVoVlHvE5yAjANBgkqhkiG9w0BAQwFADBE
      MQswCQYDVQQGEwJOTDEZMBcGA1UEChMQR0VBTlQgVmVyZW5pZ2luZzEaMBgGA1UE
      AxMRR0VBTlQgRVYgUlNBIENBIDQwHhcNMjIxMjA1MDAwMDAwWhcNMjMxMjA1MjM1
      OTU5WjCBtTEaMBgGA1UEBRMRR292ZXJubWVudCBFbnRpdHkxEzARBgsrBgEEAYI3
      PAIBAxMCR0IxGjAYBgNVBA8TEUdvdmVybm1lbnQgRW50aXR5MQswCQYDVQQGEwJH
      QjESMBAGA1UECBMJV2lsdHNoaXJlMSMwIQYDVQQKExpVSyBSZXNlYXJjaCBhbmQg
      SW5ub3ZhdGlvbjEgMB4GA1UEAxMXc2VydmljZTMubnViZXMucmwuYWMudWswggIi
      MA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCrg8uLGrLva3rliWraAxJsbRk5
      8JqfEoHCaSd/g6Zkf6pjqepQPRtkqsNDRDv68a1ltzpp1QVXiasCRTonfLUqIVsd
      fLB/Vres4PRY9foYaP8p+a+5lo5HmmKxIpTCALFZiwLs2/lyr/qaUysJBjOYrOXQ
      /d1c3M6cPp5D8icWpTH3YMhsVLhvLsElOszS/LiD7UGo5Csb5b64y7Lt3azEmoEd
      hwEQg+ExQ94cL7LD6AP8w91anNGp/s1XFqKicPLNiCuKsCDJ/ZWkd8ie7hEujZqn
      yrvzPnrQwdFymS/QBxHlsG+YN7RkfWVtLQWZRN45EFgp7kpa0AwUK4ONAOMalOeR
      4BIWnHGVUc6BPf39scVAG4nhTEI2iYOvZx0W5Y65zyjzNfl7Uk4xb9clBTKINmHf
      +AEaLX7uFgK5SaZCuxrkqT38AOheUe3QXCIFBUbWTj72G801AEDlxCuu8lLSSALh
      0XdOpCpFavTqgGefXUS+/Q919CQZQHFqKXK69NS1ao138DAC0Kz654fOFC1IpBG+
      LFpkvt77qq7/kSR99U+2qAS36YAxZ9VVlQQibNvGHzMZUaeEXAubmlPBPc0OUsdo
      YEwnRTkqHLfIiIlCvVxyWQzP14thREW0r35iWmlHeCRMFhSbR7o3ehZ0mj2nIX/i
      wjrtBG+4YfSM4yfWEQIDAQABo4IDzTCCA8kwHwYDVR0jBBgwFoAUtiAOrqPL6VUD
      BhNm1Ky+J5BUYPMwHQYDVR0OBBYEFDA64MSZqnj1BnffiEpadakRbJT2MA4GA1Ud
      DwEB/wQEAwIFoDAMBgNVHRMBAf8EAjAAMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggr
      BgEFBQcDAjBJBgNVHSAEQjBAMDUGDCsGAQQBsjEBAgEFATAlMCMGCCsGAQUFBwIB
      FhdodHRwczovL3NlY3RpZ28uY29tL0NQUzAHBgVngQwBATA/BgNVHR8EODA2MDSg
      MqAwhi5odHRwOi8vR0VBTlQuY3JsLnNlY3RpZ28uY29tL0dFQU5URVZSU0FDQTQu
      Y3JsMHUGCCsGAQUFBwEBBGkwZzA6BggrBgEFBQcwAoYuaHR0cDovL0dFQU5ULmNy
      dC5zZWN0aWdvLmNvbS9HRUFOVEVWUlNBQ0E0LmNydDApBggrBgEFBQcwAYYdaHR0
      cDovL0dFQU5ULm9jc3Auc2VjdGlnby5jb20wggF9BgorBgEEAdZ5AgQCBIIBbQSC
      AWkBZwB1AK33vvp8/xDIi509nB4+GGq0Zyldz7EMJMqFhjTr3IKKAAABhOLIDWwA
      AAQDAEYwRAIgabdDW4rh+WM2+niVu0pKTVGbUJ5oKyPKRTIffg4ty7UCICf8uTVV
      1/jI+69FeFhYsyv9cts2IO4B6yvkApyGh3QCAHcAVYHUwhaQNgFK6gubVzxT8MDk
      OHhwJQgXL6OqHQcT0wwAAAGE4sgNiAAABAMASDBGAiEAhaAQqo2pFc26jI5BfDjV
      cqFiytkCZA5eUScyZugl+igCIQDgAOMX9eK4pUCW+KHEhZqR06jiQ8SM33aT0eOk
      Q/xfmgB1AHoyjFTYty22IOo44FIe6YQWcDIThU070ivBOlejUutSAAABhOLIDS4A
      AAQDAEYwRAIgbeBb2dvd2JAa4+TJ58xvL2xX6KRq/TdDPAEwyWmvy+0CICSL8/QC
      HUz0ewuTUnal6CUmwANM3kMnBvdhTerRAy6IMIHFBgNVHREEgb0wgbqCF3NlcnZp
      Y2UzLm51YmVzLnJsLmFjLnVrghRjbG91ZC5udWJlcy5ybC5hYy51a4IWY2xvdWQu
      bnViZXMuc3RmYy5hYy51a4IQY2xvdWQuc3RmYy5hYy51a4ITbmV3Y2xvdWQuc3Rm
      Yy5hYy51a4IYb3BlbnN0YWNrLm51YmVzLnJsLmFjLnVrghpvcGVuc3RhY2subnVi
      ZXMuc3RmYy5hYy51a4IUb3BlbnN0YWNrLnN0ZmMuYWMudWswDQYJKoZIhvcNAQEM
      BQADggIBAC/iSuXvjsIui/VGjp2FvSrUGWvj59g47nFcrQW2wHWrtT+BKTKCAaLD
      1lMMwc3oeGmdoFkq5m/nSo+MvDtd2kijWgtCEK5Rj790SorwH4oT/hLDeYnZAgfd
      r387xdJxDRv1a+lyjmytHaGrOoNzEWZaIgQmzk/RRq3OuJ0wOVDFaH5a7678gWog
      6EsXMSU94kUAMZ1rt+m+BypVRSYjdGDiXbTOdp4HavadsvgqdpyPPFtierFjXGqg
      ZccUTBDBkif8VQzq7f0hT+FClH0104DE7CvhYyuAHMS4T6a4vIgw3wJ4vXsfOe71
      htuOOQ9DPm+VhHPrHaEM1b6BPDuZwcjX9EC7KvTItRAMpdvMKNGfyttRnAbAUpTh
      fP+PIeIWH/Y4Zjk7WwtI4QvKW1ifGFDwdmdtt+PulaY4zHv2s5icegfjO5WuCT2c
      VJ3Qe4GlZzTVkw625qfOnie0Lj+8zXXK/RQ/Xb8A3voQ8Ph209jertMEYxCzaHd5
      +cfPc9LZy9MNZY8WAwteQi4CNOQV0aPEE5bnMkoAkFjYfIpFFknFZ947EmmMPyvk
      g5TJxbAHoPcV7UzjlauU4YDZY3pj5bRIRLrzKwwLZNrpBahOs+nhTGEHAtFKtnCP
      kc83Pc0FYYKi81E/hqCWfXsxu4JRZ+nRWok3CYTjJGx3nMJUowYX
      -----END CERTIFICATE-----
      -----BEGIN CERTIFICATE-----
      MIIG5DCCBMygAwIBAgIQBHuLbQmxZWdCqKfBhpyfqjANBgkqhkiG9w0BAQwFADCB
      iDELMAkGA1UEBhMCVVMxEzARBgNVBAgTCk5ldyBKZXJzZXkxFDASBgNVBAcTC0pl
      cnNleSBDaXR5MR4wHAYDVQQKExVUaGUgVVNFUlRSVVNUIE5ldHdvcmsxLjAsBgNV
      BAMTJVVTRVJUcnVzdCBSU0EgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMjAw
      MjE4MDAwMDAwWhcNMzMwNTAxMjM1OTU5WjBEMQswCQYDVQQGEwJOTDEZMBcGA1UE
      ChMQR0VBTlQgVmVyZW5pZ2luZzEaMBgGA1UEAxMRR0VBTlQgRVYgUlNBIENBIDQw
      ggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCfQj5WwXIGcoT7zvITdpnR
      8P7auxwyAmBFjTtOksiLjlXP62wum102dz70U1cvdWnrK3e32SyVHIvVVJRIeUIl
      q9cRj38gd/4t/e2etn+tpHUtL9wK5AIXs9tpIjEnw4UlcpLsVpLEiyc1Sc2XtwbC
      or7MMrOB4qVucNPF+GQHWi/Vnss5zLVzXlfuJ7A5Efx1lSeWvMkNDdHxxuv62yYs
      j+FEiY1IXjV1WyWxTlseJcazDUciw7LMK30Tp9smdONuPX6rwEVSTUiC3QzbwNaP
      4C1Um2yAEbdve4Ru/qURloHud5LkBaJTm2+MDJz+yHZf4g7lvKpbcKrf59Jg8h02
      sRipT6GA3cjpgB5RU5bnXFkn4kpT2+/P25HQFeg0lyKw8dFm864sn8a37GrWviOC
      TOCWtrrSxtvLvCy9axxgc9btrA596rMKrw2tpp42KPPRXHjaVqfLf6Wj2SCynFWx
      +insGBIikpbxxOdrt4qGEDgVdSOCN9f3b6CgyTs7PD5KlUiei05Cg7sbX/Zi9DE0
      8782f4SGsZtyn4G/rMIqxyZ8SfyQZV17c+zNkttdy93iQv+IlNJGalCM56Mmkv5n
      gRtWJXzk0stw30tVWdRSGlRVxxScfcwEF1GcfVjuyosTSnEju61Qi9N5WvhqitIX
      J/wLzkppJc77xbQX+5tinwIDAQABo4IBizCCAYcwHwYDVR0jBBgwFoAUU3m/Wqor
      Ss9UgOHYm8Cd8rIDZsswHQYDVR0OBBYEFLYgDq6jy+lVAwYTZtSsvieQVGDzMA4G
      A1UdDwEB/wQEAwIBhjASBgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdJQQWMBQGCCsG
      AQUFBwMBBggrBgEFBQcDAjA4BgNVHSAEMTAvMC0GBFUdIAAwJTAjBggrBgEFBQcC
      ARYXaHR0cHM6Ly9zZWN0aWdvLmNvbS9DUFMwUAYDVR0fBEkwRzBFoEOgQYY/aHR0
      cDovL2NybC51c2VydHJ1c3QuY29tL1VTRVJUcnVzdFJTQUNlcnRpZmljYXRpb25B
      dXRob3JpdHkuY3JsMHYGCCsGAQUFBwEBBGowaDA/BggrBgEFBQcwAoYzaHR0cDov
      L2NydC51c2VydHJ1c3QuY29tL1VTRVJUcnVzdFJTQUFkZFRydXN0Q0EuY3J0MCUG
      CCsGAQUFBzABhhlodHRwOi8vb2NzcC51c2VydHJ1c3QuY29tMA0GCSqGSIb3DQEB
      DAUAA4ICAQAmUg8HhUdEzgbJQrk3G7JLuFyKJHWQ74FRHMFCorKd+h9TXoxSBOAo
      PZU5iIKv/mPXyhT+sh75NvlXr/N+d52Tx2HIY3IYWx82I0uaa0/tPKImL6ByJuqu
      mcvb2c4MqCfTGAi8kILbQt3WHK/idEiG2APMlq1YNoR3aBh7tc1rloxmQ5joEmrj
      wSoNA60AV2w9zEmi4KTYVZzsTjRgK8vh8WGQ4un3ZQhRYIVTdiI1J63kohIcpFTe
      fJyz9ip3C1Rjr5kink4bGlcxpRb1WjEs3OWhzCJSo7/ZZIIZu0pRsDKltkpNmoUQ
      5/4aIAEG9a9azLTKxTktcKOEJCOzdVIhNC/b6jrCFD7vR9v/dTI8zX9Ol1cK2nms
      k/uD5CZcvMZ6Rh/D7AS7B8dWODbN3nQPzucgdION4KHvJJe1G2SxDe+5MQO4CbwI
      WI5U+OnxKTP3vlJv2CxvP/+4Drf3hkp7YvVSZnmc9KlOeDvdC3Om9jNSw3njrEde
      foNq1eeeV89hCWKKdn05kpyitOiy7pTJJuDgVs64YGaSdQZG0E4m1bMSo13cQxYg
      J6EY3nagUJyiLzdMNuhbKlj2oXxjGLgUXYJJgSnnjqK7ldTbJDXK/qVX0gQSOr1b
      72T77sRtDcLsmkh7hYi6a+QwKARgmMLZjDirRS6ovWtH60yG2rkxBw==
      -----END CERTIFICATE-----
      -----BEGIN CERTIFICATE-----
      MIIF3jCCA8agAwIBAgIQAf1tMPyjylGoG7xkDjUDLTANBgkqhkiG9w0BAQwFADCB
      iDELMAkGA1UEBhMCVVMxEzARBgNVBAgTCk5ldyBKZXJzZXkxFDASBgNVBAcTC0pl
      cnNleSBDaXR5MR4wHAYDVQQKExVUaGUgVVNFUlRSVVNUIE5ldHdvcmsxLjAsBgNV
      BAMTJVVTRVJUcnVzdCBSU0EgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTAw
      MjAxMDAwMDAwWhcNMzgwMTE4MjM1OTU5WjCBiDELMAkGA1UEBhMCVVMxEzARBgNV
      BAgTCk5ldyBKZXJzZXkxFDASBgNVBAcTC0plcnNleSBDaXR5MR4wHAYDVQQKExVU
      aGUgVVNFUlRSVVNUIE5ldHdvcmsxLjAsBgNVBAMTJVVTRVJUcnVzdCBSU0EgQ2Vy
      dGlmaWNhdGlvbiBBdXRob3JpdHkwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIK
      AoICAQCAEmUXNg7D2wiz0KxXDXbtzSfTTK1Qg2HiqiBNCS1kCdzOiZ/MPans9s/B
      3PHTsdZ7NygRK0faOca8Ohm0X6a9fZ2jY0K2dvKpOyuR+OJv0OwWIJAJPuLodMkY
      tJHUYmTbf6MG8YgYapAiPLz+E/CHFHv25B+O1ORRxhFnRghRy4YUVD+8M/5+bJz/
      Fp0YvVGONaanZshyZ9shZrHUm3gDwFA66Mzw3LyeTP6vBZY1H1dat//O+T23LLb2
      VN3I5xI6Ta5MirdcmrS3ID3KfyI0rn47aGYBROcBTkZTmzNg95S+UzeQc0PzMsNT
      79uq/nROacdrjGCT3sTHDN/hMq7MkztReJVni+49Vv4M0GkPGw/zJSZrM233bkf6
      c0Plfg6lZrEpfDKEY1WJxA3Bk1QwGROs0303p+tdOmw1XNtB1xLaqUkL39iAigmT
      Yo61Zs8liM2EuLE/pDkP2QKe6xJMlXzzawWpXhaDzLhn4ugTncxbgtNMs+1b/97l
      c6wjOy0AvzVVdAlJ2ElYGn+SNuZRkg7zJn0cTRe8yexDJtC/QV9AqURE9JnnV4ee
      UB9XVKg+/XRjL7FQZQnmWEIuQxpMtPAlR1n6BB6T1CZGSlCBst6+eLf8ZxXhyVeE
      Hg9j1uliutZfVS7qXMYoCAQlObgOK6nyTJccBz8NUvXt7y+CDwIDAQABo0IwQDAd
      BgNVHQ4EFgQUU3m/WqorSs9UgOHYm8Cd8rIDZsswDgYDVR0PAQH/BAQDAgEGMA8G
      A1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQEMBQADggIBAFzUfA3P9wF9QZllDHPF
      Up/L+M+ZBn8b2kMVn54CVVeWFPFSPCeHlCjtHzoBN6J2/FNQwISbxmtOuowhT6KO
      VWKR82kV2LyI48SqC/3vqOlLVSoGIG1VeCkZ7l8wXEskEVX/JJpuXior7gtNn3/3
      ATiUFJVDBwn7YKnuHKsSjKCaXqeYalltiz8I+8jRRa8YFWSQEg9zKC7F4iRO/Fjs
      8PRF/iKz6y+O0tlFYQXBl2+odnKPi4w2r78NBc5xjeambx9spnFixdjQg3IM8WcR
      iQycE0xyNN+81XHfqnHd4blsjDwSXWXavVcStkNr/+XeTWYRUc+ZruwXtuhxkYze
      Sf7dNXGiFSeUHM9h4ya7b6NnJSFd5t0dCy5oGzuCr+yDZ4XUmFF0sbmZgIn/f3gZ
      XHlKYC6SQK5MNyosycdiyA5d9zZbyuAlJQG03RoHnHcAP9Dc1ew91Pq7P8yF1m9/
      qS3fuQL39ZeatTXaw2ewh0qpKJ4jjv9cJ2vhsE/zB+4ALtRZh8tSQZXq9EfX7mRB
      VXyNWQKV3WKdwrnuWih0hKWbt5DHDAff9Yk2dDLWKMGwsAvgnEzDHNb842m1R0aB
      L6KCq9NjRHDEjf8tM7qtj3u1cIiuPhnPQCjY/MiQu12ZIvVS5ljFH4gxQ+6IHdfG
      jjxDah2nGN59PRbxYvnKkKj9
      -----END CERTIFICATE-----
    ```

8. Add `export OS_CLOUD=openstack` in `~/.bashrc`.

9. Install kubectl:

   ```bash
   $ curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
   $ sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
   ```

10. Clone this repository & associated configuration repository + initialise the dependent repository (private):

    ```bash
    $ yum install cmake
    $ git clone https://gitlab.com/ska-telescope/src/deployments/skaosrc/ska-src-skaosrc-nodes-cfg
    $ git clone https://gitlab.com/ska-telescope/src/deployments/skaosrc/ska-src-skaosrc-nodes-iac
      $ cd ska-src-skaosrc-nodes-iac && git submodule init && git submodule update && cd ..
    ```

## Quickstarts

### Creating a new machine on the SRCNet network

1. Create the machine:

   ```bash
   $ make -f machine-management.mk create-and-configure-new-machine REQUEST_MACHINE_NAME=srcnet-test-machine-1 REQUEST_MACHINE_IMAGE=rocky-9-nogui REQUEST_MACHINE_FLAVOR=l3.tiny REQUEST_MACHINE_SSH_USER=root
   ```

   Remember to change the `REQUEST_MACHINE_SSH_USER` to be the corresponding distros root user, e.g. for ubuntu use `ubuntu`, for redhat machines use `root`.

2. Update the hosts on the DNS machine:

   ```bash
   $ make -f dns.mk update-hosts
   ```

3. Add a new entry to your `.ssh/config`:

   ```
   Host <testing_machine_hostname>
     User <user>
     ProxyJump <jump_machine_hostname>
   ```

   Ensuring that the `testing_machine_hostname` is resolvable from the jump machine. If an entry for the jump machine 
   from the `ProxyJump` directive doesn't already exist, add this also, e.g. 

   ```
   Host <jump_machine_hostname>
     HostName <jump_machine_ip>
     User <user>
     IdentityFile ~/.ssh/id_rsa
   ```

## Bastion machine (bastion.mk)

The targets in this Makefile facilitate the creation of a bastion machine (or jump host). This machine serves as the 
entrypoint to all other SKAOSRC SRCNet machines.

The playbooks require that the user has access to the private configuration repository, `skaosrc/ska-skaosrc-nodes-cfg`, 
containing information on the accounts to be created on the machine. By default, it is assumed that this configuration 
repository has been cloned at the root of the `ska-skaosrc-nodes-iac`. If this is not the case, the Makefile variables 
`COMMON_CFG_DIR_ABSPATH` and `WORKING_DIR_ABSPATH` will need to be adjusted accordingly.

### Playbook listing

All bastion related playbooks are kept under the folder `bastion/playbooks`:

| Playbook                                | Description                                                  |
| --------------------------------------- | ------------------------------------------------------------ |
| create_bastion_security_group_openstack | Create a security group for the proxy machine(s) with ingress rules on Openstack |
| create_bastion_machines_openstack       | Create instance(s) of the proxy machine on Openstack         |
| configure_bastion_machines              | Configure bastion machine(s) e.g. nameservers, accounts      |

### Creating a bastion machine

To create the Bastion machine(s), run the `all` Makefile target:

```bash
$ make -f bastion.mk all
```

#### Configuring the number of machines

To configure the number of bastion machines created, add/delete roles from the `create_bastion_machines_openstack playbook in `bastion/playbooks`.

## Proxy machine (proxy.mk)

The targets in this makefile facilitate the creation of machines running the HAProxy reverse proxy service. 

The corresponding playbooks require that the user has access to the private configuration repository, 
`skaosrc/ska-skaosrc-nodes-cfg`, containing both information on the accounts to be created on the machine, and the 
HAProxy service configuration (certificates, userlists, frontends, backends) for SKAOSRC. By default, it is assumed 
that this configuration repository has been cloned at the root of the `ska-skaosrc-nodes-iac`. If this is not the case, 
the Makefile variables `PROXY_CFG_DIR_ABSPATH` and `COMMON_CFG_DIR_ABSPATH` will need to be adjusted accordingly.

### Playbook listing

All proxy related playbooks are kept under the folder `proxy/playbooks`:

| Playbook                              | Description                                                  |
| ------------------------------------- | ------------------------------------------------------------ |
| create_proxy_security_group_openstack | Create a security group for the proxy machine(s) with ingress rules on Openstack |
| create_proxy_machines_openstack       | Create instance(s) of the proxy machine on Openstack         |
| configure_proxy_machines              | Configure proxy machine(s) e.g. nameservers, accounts        |
| install_or_update_haproxy             | Install or update the HAProxy service on a machine           |
| install_or_renew_certs                | Install certificates on a machine running the HAProxy service (assumes installation via`install_or_update_haproxy` |

### Creating an HAProxy machine

To create a HAProxy machine, run the `all` Makefile target:

```bash
$ make -f proxy.mk all
```

#### Configuring the number of machines

To configure the number of bastion machines created, add/delete roles from the `create_proxy_machines_openstack.yml` 
playbook in `proxy/playbooks`. If creating multiple machines, these will currently run independently of each other; 
there is no configured redundancy (not very HA, I know).

### Updating certificates

If HAProxy machines need to be updated with a new set of subdomains, edit the configuration in 
`ska-skaosrc-nodes-cfg/haproxy/etc`, ensuring there is a new backend in `backends` and corresponding frontend in 
`frontends/all-80-443`, as well as an up to date list of subdomains in `certificate.yml`. To update the HAProxy 
configuration, run the `install-haproxy` Makefile target:

```bash
$ make -f proxy.mk install-or-update-haproxy
```

Then, to renew the certificates, run the `install-or-renew-certs` Makefile target:

```bash
$ make -f proxy.mk install-or-renew-certs
```

## DNS machine (dns.mk)

The targets in this makefile facilitate the creation of machines running the dnsmasq DNS service. 

The corresponding playbooks require that the user has access to the private configuration repository, 
`skaosrc/ska-skaosrc-nodes-cfg`, containing both information on the accounts to be created on the machine, and the 
dnsmasq service configuration (additional hosts) for SKAOSRC. By default, it is assumed that this configuration 
repository has been cloned at the root of the `ska-skaosrc-nodes-iac`. If this is not the case, the Makefile variables 
`DNS_CFG_DIR_ABSPATH`, `COMMON_CFG_DIR_ABSPATH` and `WORKING_DIR_ABSPATH` will need to be adjusted accordingly.

### Playbook listing

All DNS related playbooks are kept under the folder `dns/playbooks`:

| Playbook                            | Description                                                  |
| ----------------------------------- | ------------------------------------------------------------ |
| create_dns_security_group_openstack | Create a security group for the DNS  machine(s) with ingress rules on Openstack |
| create_dns_machines_openstack       | Create instance(s) of the DNS machine on Openstack           |
| configure_dns_machines              | Configure DNS machine(s) e.g. nameservers, accounts          |
| install_dnsmasq                     | Install the dnsmasq service on a machine                     |
| update_hosts                        | Update `resolv.conf` from machine inventory (assumes installation via`install_dnsmasq`) |

### Creating a dnsmasq machine

To create a dnsmasq machine, run the `all` Makefile target:

```bash
$ make -f dns.mk all
```

#### Configuring the number of machines

To configure the number of bastion machines created, add/delete roles from the `create_dns_machines_openstack.yml` 
playbook in `dns/playbooks`. 

### Updating hosts

If the machine inventory is updated, these changes should be pushed to the DNS machine(s). Additional hosts can be added 
by editing the configuration in  `ska-skaosrc-nodes-cfg/dnsmasq/etc/additional_hosts` and running the `update-hosts` 
Makefile target:

```bash
$ make -f dns.mk update-hosts
```

## Machine management (machine-management.mk)

The targets in this makefile facilitate the creation/deletion of general machines. This method should be used in place 
of any CLI/GUI tool, so that the resulting machine can be added to/deleted from the tracked machine inventory.

The corresponding playbooks require that the user has access to the private configuration repository, 
`skaosrc/ska-skaosrc-nodes-cfg`, containing information on the accounts to be created on the machine. By default, it is 
assumed that this configuration repository has been cloned at the root of the `ska-skaosrc-nodes-iac`. If this is not 
the case, the Makefile variables `COMMON_CFG_DIR_ABSPATH` and `WORKING_DIR_ABSPATH` will need to be adjusted 
accordingly.

### Playbook listing

All machine management related playbooks are kept under the folder `machine-management/playbooks`:

| Playbook                         | Description                                          |
| -------------------------------- | ---------------------------------------------------- |
| delete_general_machine_openstack | Delete instance of a general machine on Openstack    |
| create_general_machine_openstack | Create instance of a general machine on Openstack    |
| configure_machine                | Configure general machine e.g. nameservers, accounts |

### Creating a general machine

To create a general machine, run the `create-and-configure-new-machine` Makefile target, overriding machine parameters 
as required, e.g.

```bash
$ make -f machine-management.mk create-and-configure-new-machine REQUEST_MACHINE_NAME=srcnet-test-machine-1 REQUEST_MACHINE_IMAGE=rocky-9-nogui REQUEST_MACHINE_FLAVOR=l3.tiny REQUEST_MACHINE_SSH_USER=root
```

As the playbooks used in this Makefile are generic, it is highly recommended not to edit the Makefile with these 
parameters directly lest playbooks be accidentally applied to incorrect machines. This is especially true for the 
`REQUEST_MACHINE_NAME` parameter.

## User management (user-management.mk)

The targets in this makefile facilitate the creation/deletion of users on all machines tracked by the machine inventory.

The corresponding playbooks require that the user has access to the private configuration repository, 
`skaosrc/ska-skaosrc-nodes-cfg`, containing information on the accounts to be created on/deleted from the machines. 
By default, it is assumed that this configuration repository has been cloned at the root of the `ska-skaosrc-nodes-iac`. 
If this is not the case, the Makefile variables `COMMON_CFG_DIR_ABSPATH` and `WORKING_DIR_ABSPATH` will need to be 
adjusted accordingly.

### Playbook listing

All user management related playbooks are kept under the folder `user-management/playbooks`:

| Playbook         | Description                                 |
| ---------------- | ------------------------------------------- |
| delete_user_all  | Delete user account from all machines       |
| update_users_all | Update/create user accounts on all machines |

### Creating/updating/deleting user(s)

To create/update a user on all machines, run the `update-users-all` Makefile target e.g.

```bash
$ make -f user-management.mk update-users-all
```

To delete a single user on all machines, run the `delete-user-all` Makefile target, overriding user parameters as 
required, e.g.

```bash
$ make -f user-management.mk delete-user-all USER_ACCOUNT=
```

As the playbooks used in this Makefile are generic, it is highly recommended not to edit the Makefile with these 
parameters directly lest playbooks be accidentally applied to incorrect users. 

## Cluster API management and workload machines (capi.mk)

The targets in this Makefile facilitate Cluster API based Kubernetes deployments using the `clusterapi` collection 
from `ska-ser-ansible-collections.ska_collections` (https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections), 
requiring additional roles from the `ska_collections.minikube` collection for building a management cluster with 
Minikube and the `ska_collections.k8s` collection for post workload cluster deployment processing and customisation.

The Cluster API operator is an operator that works on the same principles as any other Kubernetes custom resource 
definition. The operator is deployed in a "management cluster" along with the desired cloud infrastructure providers, 
e.g. [the openstack cluster api provider](https://github.com/kubernetes-sigs/cluster-api-provider-openstack) that provide the driver interface for communicating with the specific 
infrastructure context. See the [clusterapi reference]( https://cluster-api.sigs.k8s.io/user/concepts.html) for details.

To create a "workload cluster" the user defines a collection of manifests that describe the machine and cluster 
layout. The corresponding manifest is then generated using `clusterctl generate cluster` and applied to the management 
cluster, which in turn orchestrates the creation of the workload cluster by communicating with the infrastructure 
provider (openstack in this case) and driving the [kubeadm configuration manager](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/).

The Cluster API manifest specification enables a set of "pre" and "post" kubeadm init hooks that are applied to both 
the control plane and worker nodes in the target workload cluster. These hooks enable customisations of the deployment 
to be injected into the deployment workflow. This cannot be achieved by the `clusterctl generate cluster` flow directly, 
so [kustomize](https://kustomize.io/) templates are used to inject the necessary changes. These templates add in the execution of specific 
ansible-playbook flows for both the control plane and worker nodes so that the hosts are customised and the necessary 
baseline services are installed into the workload cluster e.g. containerd mirror configs, docker, helm tools, pod 
networking etc.

### Playbook listing

CAPI related playbooks are kept under either the folder `capi_management/playbooks` or `capi_workload/playbooks` 
depending on whether the action is for the management or workload machine respectively.

#### Management

| Playbook         | Description                                                                          |
| ---------------- |--------------------------------------------------------------------------------------|
| capi_management_containers  | Install containerisation technologies on a CAPI management cluster |
| capi_management_minikube | Install minikube on a CAPI management cluster |
| capi_management_capo | Install clusterctl and the CAPO infrastructure provider on a CAPI management cluster |
| capi_management_post | Add management cluster backups to crontab |
| create_capi_management_machine_openstack | Create an instance for the CAPI management machine on Openstack |
| delete_capi_management_machine_openstack | Delete an instance of the capi management machine on Openstack |

#### Workload

| Playbook         | Description                                                                                       |
| ---------------- |---------------------------------------------------------------------------------------------------|
| capi_workload_deploy  | Create a CAPI workload cluster using the `ska_collections`.`clusterapi`.`createworkload` playbook |
| capi_workload_post | Deploy integral services on a CAPI workload cluster, e.g. ingress                                 |
| capi_workload_delete | Delete a CAPI workload cluster                                                                    ||

### Creating the management cluster

From the infrastructure machine, the deployment workflow proceeds via Makefiles targets. A full end-to-end example is 
shown below.

First we need to create the machine that will be used to host the management cluster:

```bash
  $ make -f capi.mk  create-capi-management-machine-openstack
```

The intermediate working directory (e.g. where inventory files will be stored) can either be specified in the Makefile 
or the role defaults.

Next, create the management cluster:

```bash
  $ make -f capi.mk capi-management
```

The management cluster attributes can either be specified directly in the Makefile or passed in as variables.

Finally, you may want to add user accounts and DNS entries for this cluster. As ansible manages the entry for this machine in `inventory.ini` , this is simply a case of running the corresponding Makefile targets.

### Creating a workload cluster

To create a workload cluster:

```bash
  $ make -f capi.mk capi-workload
```

The workload cluster attributes can either be specified directly in the Makefile or passed in as variables.

You may want to add user accounts and DNS entries for this cluster. As ansible does not manage the entries for resulting machines in `inventory.ini` , you must first add them manually. It is then simply a case of running the corresponding Makefile targets.

As part of the workload cluster creation process, both an ingress controller (with optional loadbalancer) and argocd are deployed.

### Notes

- Make sure that, if a DNS machine is used, that it is contactable (has the correct security groups set to allow ingress on port 53) otherwise the deployment hooks will fail. This behaviour can be observed in `/tmp/capo-config.log`.

- Need two versions of `clouds.yaml`. When creating the management machine, the one at `/etc/openstack/clouds.yaml` is 
  used. When driving clusterapi, the one specified in `capi_workload_deploy` playbook (`capi_capo_openstack_cloud_config`)
  is used. This is because the key `cacert` is required by the latter but prohibited by the former.

- Capi images are not built beforehand; they are instead made on the fly against vanilla ubuntu images with kubeadm 
  pre/post init hooks using kustomize - see [here](https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections/-/tree/main/resources/clusterapi/kustomize/capobase). These kustomize snippets are merged together before being put 
  through `clusterctl` to generate the workload cluster manfests. See `/tmp/capo-config.log` on the control/worker plane f
  for logs. Likewise, pod networking is done after join with post init hooks. *Trying to use capi specific images has 
  previously led to errors where kubernetes tries to use docker for networking rather than containerd*.
