# TODO

- [ ] Need equivalent playbooks for infrastructure-manager machine
- [ ] Proxy playbook should spawn HA HAProxy service.
- [ ] Fix CAPI playbooks & refactor to move variables from Makefile into playbook (see other playbooks for example).
