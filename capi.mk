.PHONY: all create-capi-management-machine-openstack capi-management capi-workload pre

CAPI_MANAGEMENT_MACHINE_NAME 			?= srcnet-capi-mgmt-cluster-1#		
CAPI_MANAGEMENT_MACHINE_FLOATING_IP    		?= #                                    must be available in the pool
CAPI_MANAGEMENT_MACHINE_IMAGE           	?= ubuntu-jammy-22.04#
CAPI_MANAGEMENT_MACHINE_FLAVOR          	?= l3.tiny#
CAPI_MANAGEMENT_MACHINE_SSH_USER       		?= ubuntu#
CAPI_MANAGEMENT_MACHINE_KEY_NAME        	?= root-srcnet-infrastructure-provisioner#
CAPI_MANAGEMENT_MACHINE_NETWORK         	?= SRCNet-network#
CAPI_MANAGEMENT_MACHINE_AUTO_IP         	?= no#
CAPI_WORKLOAD_CLUSTER_NAME			?= srcnet-capi-wkld-cluster-1# 
CAPI_WORKLOAD_CLUSTER_LOADBALANCER_SUFFIX       ?= srcnet-capi-wkld-lb-1#
CAPI_WORKLOAD_CLUSTER_CONTROLPLANE_COUNT        ?= 3#
CAPI_WORKLOAD_CLUSTER_WORKER_COUNT              ?= 6#
CAPI_WORKLOAD_CLUSTER_INGRESS                   ?= true#                               whether ingress should be installed/reinstalled on the workload cluster. Set to false if ingress already exists and you don't want to recreate ingress-nginx controller, LB, etc
CAPI_WORKLOAD_CLUSTER_ARGOCD_HOST		?= argocd.srcnet.skao.int
CAPI_WORKLOAD_MACHINES_IMAGE                    ?= ubuntu-jammy-22.04#
CAPI_WORKLOAD_MACHINES_KEY_NAME                 ?= root-srcnet-infrastructure-provisioner#
CAPI_WORKLOAD_MACHINES_EXTERNAL_NETWORK_ID      ?= 5283f642-8bd8-48b6-8608-fa3006ff4539#
CAPI_WORKLOAD_MACHINES_OS_NETWORK_NAME          ?= SRCNet-network#
CAPI_WORKLOAD_MACHINES_OS_SUBNET_NAME           ?= SRCNet-network-subnet#
CAPI_WORKLOAD_MACHINES_CONTROL_PLANE_FLAVOR     ?= l3.xsmall#
CAPI_WORKLOAD_MACHINES_WORKER_FLAVOR            ?= l3.xsmall#

WORKING_DIR_ABSPATH             		?= /opt/ska-src-skaosrc-nodes-cfg#      where inventories etc. will be kept
CAPI_MANAGEMENT_TARGET_HOSTS_GROUP		?= capi_cluster_1#			the target group name for the management cluster in the inventories file, create a new one per cluster
CAPI_MANAGEMENT_WORKLOAD_KUBECONFIG_DIR		?= /etc/kubeconfigs#			where workload kubeconfigs will be kept on management cluster, leave blank for default defined in role defaults
CAPI_RESOURCES_DIR				?= /opt/ska-src-skaosrc-nodes-iac/ska-ser-ansible-collections/resources/clusterapi#		directory to store all the yaml manifests for infra deployed on/by the management cluster

# --- Do not edit below here.

ANSIBLE_EXTRA_VARS = --extra-vars working_dir_abspath=$(WORKING_DIR_ABSPATH) \
		     --extra-vars capi_management_target_hosts_group=$(CAPI_MANAGEMENT_TARGET_HOSTS_GROUP) \
		     --extra-vars capi_management_target_hosts=$(CAPI_MANAGEMENT_TARGET_HOSTS_GROUP) \
		     --extra-vars capi_resources_dir=$(CAPI_RESOURCES_DIR)
ifneq ($(CAPI_MANAGEMENT_MACHINE_NAME),) 
	ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_name=$(CAPI_MANAGEMENT_MACHINE_NAME) 
endif
ifneq ($(CAPI_MANAGEMENT_MACHINE_FLOATING_IP),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_floating_ip=$(CAPI_MANAGEMENT_MACHINE_FLOATING_IP)
endif
ifneq ($(CAPI_MANAGEMENT_MACHINE_IMAGE),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_image=$(CAPI_MANAGEMENT_MACHINE_IMAGE)
endif
ifneq ($(CAPI_MANAGEMENT_MACHINE_FLAVOR),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_flavor=$(CAPI_MANAGEMENT_MACHINE_FLAVOR)
endif
ifneq ($(CAPI_MANAGEMENT_MACHINE_SSH_USER),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_ssh_user=$(CAPI_MANAGEMENT_MACHINE_SSH_USER)
endif
ifneq ($(CAPI_MANAGEMENT_MACHINE_KEY_NAME),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_key_name=$(CAPI_MANAGEMENT_MACHINE_KEY_NAME)
endif
ifneq ($(CAPI_MANAGEMENT_MACHINE_NETWORK),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_network=$(CAPI_MANAGEMENT_MACHINE_NETWORK)
endif
ifneq ($(CAPI_MANAGEMENT_MACHINE_AUTO_IP),)
	ANSIBLE_EXTRA_VARS += --extra-vars capi_management_machine_auto_ip=$(CAPI_MANAGEMENT_MACHINE_AUTO_IP)
endif
ifneq ($(CAPI_WORKLOAD_CLUSTER_NAME),)
	ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_cluster_name=$(CAPI_WORKLOAD_CLUSTER_NAME) 
endif
ifneq ($(CAPI_WORKLOAD_CLUSTER_LOADBALANCER_SUFFIX),)
	ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_cluster_loadbalancer_suffix=$(CAPI_WORKLOAD_CLUSTER_LOADBALANCER_SUFFIX)
endif
ifneq ($(CAPI_WORKLOAD_CLUSTER_CONTROLPLANE_COUNT),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_cluster_controlplane_count=$(CAPI_WORKLOAD_CLUSTER_CONTROLPLANE_COUNT)
endif
ifneq ($(CAPI_WORKLOAD_CLUSTER_WORKER_COUNT),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_cluster_worker_count=$(CAPI_WORKLOAD_CLUSTER_WORKER_COUNT)
endif
ifneq ($(CAPI_WORKLOAD_CLUSTER_INGRESS),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_cluster_ingress=$(CAPI_WORKLOAD_CLUSTER_INGRESS)
endif
ifneq ($(CAPI_WORKLOAD_CLUSTER_ARGOCD_HOST),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_cluster_argocd_host=$(CAPI_WORKLOAD_CLUSTER_ARGOCD_HOST)
endif
ifneq ($(CAPI_WORKLOAD_MACHINES_IMAGE),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_machines_image=$(CAPI_WORKLOAD_MACHINES_IMAGE)
endif
ifneq ($(CAPI_WORKLOAD_MACHINES_KEY_NAME),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_machines_key_name=$(CAPI_WORKLOAD_MACHINES_KEY_NAME)
endif
ifneq ($(CAPI_WORKLOAD_MACHINES_EXTERNAL_NETWORK_ID),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_machines_external_network_id=$(CAPI_WORKLOAD_MACHINES_EXTERNAL_NETWORK_ID)
endif
ifneq ($(CAPI_WORKLOAD_MACHINES_OS_NETWORK_NAME),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_machines_os_network_name=$(CAPI_WORKLOAD_MACHINES_OS_NETWORK_NAME)
endif
ifneq ($(CAPI_WORKLOAD_MACHINES_OS_SUBNET_NAME),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_machines_os_subnet_name=$(CAPI_WORKLOAD_MACHINES_OS_SUBNET_NAME)
endif
ifneq ($(CAPI_WORKLOAD_MACHINES_CONTROL_PLANE_FLAVOR),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_machines_control_plane_flavor=$(CAPI_WORKLOAD_MACHINES_CONTROL_PLANE_FLAVOR)
endif
ifneq ($(CAPI_WORKLOAD_MACHINES_WORKER_FLAVOR),)
        ANSIBLE_EXTRA_VARS += --extra-vars capi_workload_machines_worker_flavor=$(CAPI_WORKLOAD_MACHINES_WORKER_FLAVOR)
endif
ifneq ($(CAPI_MANAGEMENT_WORKLOAD_KUBECONFIG_DIR),)
	ANSIBLE_EXTRA_VARS += --extra-vars capi_management_workload_kubeconfig_dir=$(CAPI_MANAGEMENT_WORKLOAD_KUBECONFIG_DIR)
endif

all: create-capi-management-machine-openstack capi-management capi-workload

pre:
	ansible-galaxy collection install git+https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections.git\#ansible_collections/ska_collections/clusterapi,11be888c8ac40f9b4f9a436d50bb868881eab4db		# install the clusterapi collection from system pinned to a working commit
	ansible-galaxy collection install git+https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections.git\#ansible_collections/ska_collections/docker_base,11be888c8ac40f9b4f9a436d50bb868881eab4db		# install the docker_base collection from system pinned to a working commit
	ansible-galaxy collection install git+https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections.git\#ansible_collections/ska_collections/minikube,11be888c8ac40f9b4f9a436d50bb868881eab4db 		# install the minikube collection from system pinned to a working commit
	ansible-galaxy collection install git+https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections.git\#ansible_collections/ska_collections/instance_common,11be888c8ac40f9b4f9a436d50bb868881eab4db	# install the instance_common collection from system pinned to a working commit
	ansible-galaxy collection install git+https://gitlab.com/ska-telescope/sdi/ska-ser-ansible-collections.git\#ansible_collections/ska_collections/k8s,11be888c8ac40f9b4f9a436d50bb868881eab4db    		# install the k8s collection from system pinned to a working commit
	sed -i 's/update_repo_cache: true/update_repo_cache: false/' ~/.ansible/collections/ansible_collections/ska_collections/k8s/roles/ingress/tasks/main.yml							# otherwise helm chart update fails task
	sed -i 's/capi_capo_openstack_dns_servers: 192.168.99.162/capi_capo_openstack_dns_servers: 192.168.1.169/' ~/.ansible/collections/ansible_collections/ska_collections/clusterapi/roles/createworkload/defaults/main.yml # otherwise we get failures in DNS resolution when running deploy hooks on workload machines

create-capi-management-machine-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_management/playbooks/create_capi_management_machine_openstack.yml -vv

delete-capi-management-machine-openstack: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_ROLES_PATH=./common/playbooks/roles \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_management/playbooks/delete_capi_management_machine_openstack.yml -vv

capi-management: pre capi-management-containers capi-management-minikube capi-management-capo capi-management-post

capi-management-containers: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_management/playbooks/capi_management_containers.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv 

capi-management-minikube: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_management/playbooks/capi_management_minikube.yml --tags "build" -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv 

capi-management-capo: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_management/playbooks/capi_management_capo.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv 

capi-management-post: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
        ANSIBLE_SSH_RETRIES=10 \
        ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_management/playbooks/capi_management_post.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

capi-workload: capi-workload-deploy capi-workload-post

capi-workload-deploy: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_FILTER_PLUGINS=$(PWD)/ska-ser-ansible-collections/ansible_plugins/filter \
	ANSIBLE_LIBRARY=$(PWD)/ska-ser-ansible-collections/ansible_collections/ska_collections/clusterapi/plugins/modules \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_workload/playbooks/capi_workload_deploy.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

capi-workload-post: pre
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
	ANSIBLE_FILTER_PLUGINS=$(PWD)/ska-ser-ansible-collections/ansible_plugins/filter \
        ANSIBLE_LIBRARY=$(PWD)/ska-ser-ansible-collections/ansible_collections/ska_collections/clusterapi/plugins/modules \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_workload/playbooks/capi_workload_post.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv

capi-workload-delete: 
	ANSIBLE_HOST_KEY_CHECKING=False \
	ANSIBLE_SSH_RETRIES=10 \
	ansible-playbook $(ANSIBLE_EXTRA_VARS) ./capi_workload/playbooks/capi_workload_delete.yml -i $(WORKING_DIR_ABSPATH)/inventory.ini -vv
